#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
	paintWidget.kresliOsi(this->ui.spinBox->value());
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::KresliClicked()
{
	paintWidget.clearImage();
	paintWidget.kresliOsi(ui.spinBox->value());

	rozsah_x();
	zadaj_polynom();

	//sinus
	if (ui.comboBox->currentIndex() == 0)
	{
		if (ui.radioButton->isChecked())paintWidget.kresliSinusB(ui.spinBox->value(), rozsah);
		if (ui.radioButton_2->isChecked())paintWidget.kresliSinusC(ui.spinBox->value(), rozsah);
		if (ui.radioButton_3->isChecked())paintWidget.kresliSinusS(ui.spinBox->value(), rozsah);
	}

	//cosinus
	if (ui.comboBox->currentIndex() == 1)
	{
		if (ui.radioButton->isChecked())paintWidget.kresliSinusB(ui.spinBox->value(), rozsah, false);
		if (ui.radioButton_2->isChecked())paintWidget.kresliSinusC(ui.spinBox->value(), rozsah, false);
		if (ui.radioButton_3->isChecked())paintWidget.kresliSinusS(ui.spinBox->value(), rozsah, false);
	}

	//tangens
	if (ui.comboBox->currentIndex() == 2) {
		if (ui.radioButton->isChecked())paintWidget.kresliTangensB(ui.spinBox->value(), rozsah);
		if (ui.radioButton_2->isChecked())paintWidget.kresliTangensC(ui.spinBox->value(), rozsah);
		if (ui.radioButton_3->isChecked())paintWidget.kresliTangensS(ui.spinBox->value(), rozsah);
	}

	//cotangens
	if (ui.comboBox->currentIndex() == 3) {
		if (ui.radioButton->isChecked())paintWidget.kresliTangensB(ui.spinBox->value(), rozsah, false);
		if (ui.radioButton_2->isChecked())paintWidget.kresliTangensC(ui.spinBox->value(), rozsah, false);
		if (ui.radioButton_3->isChecked())paintWidget.kresliTangensS(ui.spinBox->value(), rozsah, false);
	}

	//polynom
	if (ui.comboBox->currentIndex() == 4) {

		if (ui.radioButton->isChecked())paintWidget.kresliPolynomB(ui.spinBox->value(), cleny, rozsah);
		if (ui.radioButton_2->isChecked())paintWidget.kresliPolynomC(ui.spinBox->value(), cleny, rozsah);
		if (ui.radioButton_3->isChecked())paintWidget.kresliPolynomS(ui.spinBox->value(), cleny, rozsah);
	}
}

void MyPainter::rozsah_x() {
	QString rozsah_string = ui.lineEdit->text();

	if (rozsah_string == "") {
		rozsah = 2;
	}
	else {
		rozsah = rozsah_string.toDouble();
	}
}

void MyPainter::zadaj_polynom() {
	QString poly = ui.lineEdit_2->text();

	QStringList split = poly.split('x');
	cleny.resize(split.length());

	int counter = 0;
	for (int i = split.length() - 1; i >= 1; i--)
	{
		QStringList tmpspl = split[i].split('+');
		if (tmpspl.length() == 2)
			cleny[counter] = tmpspl[1].toDouble();
		else
		{
			tmpspl = split[i].split('-');
			cleny[counter] = -tmpspl[1].toDouble();
		}
		counter++;
	}
	cleny[counter] = split[0].toDouble();
}