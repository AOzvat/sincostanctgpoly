#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <cmath>
#include <iostream>
#include <QLineEdit>

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void kresliOsi(int delenia);
	void kresliSinusB(int delenia, double rozsah, bool sinus = true);
	void kresliSinusC(int delenia, double rozsah, bool sinus = true);
	void kresliSinusS(int delenia, double rozsah, bool sinus = true);
	void kresliTangensB(int delenia, double rozsah, bool tangens = true);
	void kresliTangensC(int delenia, double rozsah, bool tangens = true);
	void kresliTangensS(int delenia, double rozsah, bool tangens = true);
	void kresliPolynomB(int delenia, std::vector<double> cleny, double rozsah);
	void kresliPolynomC(int delenia, std::vector<double> cleny, double rozsah);
	void kresliPolynomS(int delenia, std::vector<double> cleny, double rozsah);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
};

#endif // PAINTWIDGET_H
